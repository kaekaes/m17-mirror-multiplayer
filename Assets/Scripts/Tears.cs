﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tears : NetworkBehaviour{

	public float Dmg { get { return dmg; } set { dmg = value; } }
	protected float dmg;

	/* si la lágrima colisiona con una pared, desaparece, * 
	 * si la dispara el jugador y colisiona contra el enemigo, 
	 * llama a la función hit y hace daño al enemigo * 
	 * si la dispara el enemigo y colisiona contra el jugador, 
	 * llama a la función hit y hace daño al jugador * 
	 */
	[ServerCallback]
	private void OnTriggerEnter2D(Collider2D other) {
		if (other.transform.CompareTag("pared")) {
			Destroy(gameObject);
        }

		if(other.TryGetComponent<Golpeable>(out Golpeable p)) {
			if (transform.CompareTag("Enemy")) {
				if (other.transform.CompareTag("Player")) {
					p.Hit(dmg);
					Destroy(gameObject);
				}
			} else {
				if (!other.transform.CompareTag("Player")) {
					p.Hit(dmg);
					Destroy(gameObject);
				}
			}
		}
    }
}