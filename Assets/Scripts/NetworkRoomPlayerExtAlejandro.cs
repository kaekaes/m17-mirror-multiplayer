using UnityEngine;
using Mirror;
using UnityEngine.SceneManagement;

[AddComponentMenu("")]
    public class NetworkRoomPlayerExtAlejandro : NetworkRoomPlayer
    {
        public override void OnStartClient()
        {
            Debug.Log("OnStartClient {0}" + SceneManager.GetActiveScene().path);

            base.OnStartClient();
        }

        [ServerCallback]
        public override void OnClientEnterRoom()
        {
            Debug.Log( "OnClientEnterRoom {0}"+ SceneManager.GetActiveScene().path);
            this.readyToBegin = true;
        }

        public override void OnClientExitRoom()
        {
             Debug.Log( "OnClientExitRoom {0}"+ SceneManager.GetActiveScene().path);
        }

        public override void ReadyStateChanged(bool oldReadyState, bool newReadyState)
        {
             Debug.Log( "ReadyStateChanged {0}"+ newReadyState);
        }
    }

