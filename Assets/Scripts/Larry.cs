﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Larry : LarryCuerpo{

	public Text debugText;

    private void Start() {
        this.MoverCuerpo();
        MiPoso = 0;
		debug = debugText;
		nextPart.Repos(this);
    }

    public override void MoverCuerpo() {
		EligeSiGirar();

		base.MoverCuerpo();
    }

    private void EligeSiGirar(bool obligar = false) {
		if (pasos < 4)
			return;


		Vector2 dir = Vector2.zero;
		if (obligar) {
			debug.text += "Larry DEBE cambiar\n";
			if(Abs(miDireccion)== Vector2.up) {
				bool l = (int)Random.value == 0;
				dir = l ? -Vector2.right : Vector2.right;
			} else {
				bool u = (int)Random.value == 0;
				dir = u ? Vector2.up : -Vector2.right;
			}
		} else {
			debug.text += "Larry PUEDE cambiar\n";
			int r = Random.Range(1, obligar ? 4 : 4);
			if (r == 1 && Abs(miDireccion) != Vector2.up)
				dir = Vector2.up;
			else if (r == 2 && Abs(miDireccion) != Vector2.right)
				dir = Vector2.right;
			else if (r == 3 && Abs(miDireccion) != Vector2.up)
				dir = -Vector2.up;
			else if (r == 4 && Abs(miDireccion) != Vector2.right)
				dir = -Vector2.right;
			else
				return;
		}
		debug.text += "Larry cabeza gira: " + dir + "\n";
		Girar(dir, true);
		ResetPasos();
	}

	private void OnCollisionEnter2D(Collision2D collision){
        if(collision.Equals("pared"))
            EligeSiGirar(true);
    }

    protected override void AcabarDeMover() {
		base.AcabarDeMover();
        MoverCuerpo();
	}

	Vector2 Abs(Vector2 v) {
		return new Vector2(Mathf.Abs(v.x), Mathf.Abs(v.y));
	}
}
