﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using System;

public class LarryCuerpo : NetworkBehaviour{

    [SerializeField] protected LarryCuerpo nextPart;
    [SerializeField] protected static float timeToMove = .05f;
    [SerializeField] protected static float moveSpeednt = 30f;
	[SerializeField] protected int pasos = 0;
	protected static Text debug;



	protected int MiPoso {
        set {
            miposo = value;
            if(nextPart != null)
                nextPart.MiPoso = value + 1;
        }
        get { return miposo; }
	}
	private int miposo;

	public void ResetPasos() {
		debug.text += "Reset " + transform.name + "\n";
		pasos = 0;
		if (nextPart != null)
			nextPart.ResetPasos();
	}




    [SerializeField] protected Vector2 miDireccion = -Vector2.right;

    public virtual void MoverCuerpo() {
        StartCoroutine(Mover());
        Invoke(nameof(MoverSiguiente), timeToMove);
    }

    void MoverSiguiente() {
        if(nextPart != null)
            nextPart.MoverCuerpo();
    }

	public virtual void Girar(Vector3 direccion, bool obligar = false) {
		StartCoroutine(DeberiaGirar(direccion, obligar));
	}

	IEnumerator DeberiaGirar(Vector3 direccion, bool obligar = false) {
		//if (Vector3.Distance(transform.position, position) <= (1 / moveSpeednt)*1f || obligar) {
		if (obligar || (!obligar && pasos >= MiPoso*4)) {
			this.miDireccion = direccion;
			if (nextPart != null) {
				nextPart.Girar(direccion);
			}
			ResetPasos();
		}

		yield return new WaitForSecondsRealtime(timeToMove);
		StartCoroutine(DeberiaGirar(direccion));
	}

	IEnumerator Mover() {
        this.transform.Translate((Vector3)miDireccion * (1 / moveSpeednt));
        yield return new WaitForSecondsRealtime(timeToMove);
        AcabarDeMover();
        //MoverCuerpo();
    }

    protected virtual void AcabarDeMover() { pasos++; }

	public void Repos(LarryCuerpo papi) {
		transform.position = new Vector3(papi.transform.position.x + ((1 / moveSpeednt)*4), papi.transform.position.y, papi.transform.position.z);
		if(nextPart != null)
			nextPart.Repos(this);
	}
}
