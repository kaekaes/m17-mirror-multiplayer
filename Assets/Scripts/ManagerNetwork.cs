﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerNetwork : NetworkManager
{
	//transform de los jugadores y el boss
	public Transform P1;
    public Transform P2;
    public Transform P3;
    public Transform P4;
	public GurdyController gurdy;
	
	//lista de los jugadores
	internal static readonly List<IsaacControllerNetwork> playersList = new List<IsaacControllerNetwork>();

	//conforme se unen los jugadores, se instancian en sus posiciones correspondientes
    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        Transform start = P1;

        if (numPlayers == 1) {
			start = P2;
		} else if (numPlayers == 2) {
			start = P3;
        } else if (numPlayers == 3) {
			start = P4;
        }
		GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
		IsaacControllerNetwork p = player.GetComponent<IsaacControllerNetwork>();
		p.NumPlayers = numPlayers;
		playersList.Add(p);

		if(numPlayers == 1){
			gurdy.StartBattle(playersList);
		}
		if (numPlayers == 2 || numPlayers == 3) {
			gurdy.AddPlayer(p);
		}

		NetworkServer.AddPlayerForConnection(conn, player);
    }

	//se cierra conexión
	public override void OnServerDisconnect(NetworkConnection conn){
		//foreach (NetworkIdentity a in conn.clientOwnedObjects) {
		//	IsaacControllerNetwork p = a.transform.GetComponent<IsaacControllerNetwork>();
		//	gurdy.RemovePlayer(p);
		//}

		//print(conn+" alguien se fue");
        base.OnServerDisconnect(conn);
    }
}
