﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsaacController : MonoBehaviour{

	Animator cabesaAnim;
	Animator patasAnim;

    private Transform shootPos;

	Rigidbody2D rb;

	public float speed = 5f;
	float moveX, moveY;

    public float tearSpeed = 100f;
    public GameObject tear;

    private bool canShoot = true;

    void Start() {
		transform.GetChild(0).TryGetComponent<Animator>(out cabesaAnim);
        shootPos = transform.GetChild(0).GetChild(0);
		transform.GetChild(1).TryGetComponent<Animator>(out patasAnim);

		TryGetComponent<Rigidbody2D>(out rb);
	}

    void Update() {
		moveX = Input.GetAxis("Horizontal");
		moveY = Input.GetAxis("Vertical");

		if (cabesaAnim != null) {
			cabesaAnim.SetFloat("MoveX", moveX);
			cabesaAnim.SetFloat("MoveY", moveY);
		}
		if (patasAnim != null) {
			patasAnim.SetFloat("MoveX", moveX);
			patasAnim.SetFloat("MoveY", moveY);
		}

        if(Input.GetButton("ShootHorizontal") || Input.GetButton("ShootVertical"))
            Shoot();
	}

    private void Shoot() {
		if (cabesaAnim != null)
			cabesaAnim.SetBool("Shoot", true);
		
        float horiz = Input.GetAxisRaw("ShootHorizontal");
        float vertical = Input.GetAxisRaw("ShootVertical");

		cabesaAnim.SetFloat("MoveX", horiz);
		cabesaAnim.SetFloat("MoveY", vertical);

		print(horiz + " " + vertical);

        if(canShoot) {
			canShoot = false;
            GameObject newBullet = Instantiate(tear, shootPos);
			Rigidbody2D bullRb = newBullet.GetComponent<Rigidbody2D>();
			bullRb.position = shootPos.position;


			if (Mathf.Abs(horiz) > Mathf.Abs(vertical)) {
				bullRb.velocity = Vector2.right * tearSpeed * Time.fixedDeltaTime * horiz + (rb.velocity / 2);
				if (cabesaAnim != null) {
					cabesaAnim.SetFloat("MoveX", horiz);
					cabesaAnim.SetFloat("MoveY", 0);
				}

			} else if(Mathf.Abs(horiz) < Mathf.Abs(vertical)) {
                bullRb.velocity = Vector2.up * tearSpeed * Time.fixedDeltaTime * vertical + (rb.velocity / 2);
				if (cabesaAnim != null) {
					cabesaAnim.SetFloat("MoveX", 0);
					cabesaAnim.SetFloat("MoveY", vertical);
				}
			} else {
				if(Mathf.Abs(horiz) != 0) {
					bullRb.velocity = Vector2.right * tearSpeed * Time.fixedDeltaTime * horiz + (rb.velocity / 2);
					if (cabesaAnim != null) {
						cabesaAnim.SetFloat("MoveX", horiz);
						cabesaAnim.SetFloat("MoveY", 0);
					}
				} else if(Mathf.Abs(vertical) != 0) {
					bullRb.velocity = Vector2.up * tearSpeed * Time.fixedDeltaTime * vertical + (rb.velocity / 2);
					if (cabesaAnim != null) {
						cabesaAnim.SetFloat("MoveX", 0);
						cabesaAnim.SetFloat("MoveY", vertical);
					}

				} else {
					if (Input.GetButton("ShootHorizontal")) {
						bullRb.velocity = Vector2.right * tearSpeed * Time.fixedDeltaTime * 1 + (rb.velocity / 2);
						if (cabesaAnim != null) {
							cabesaAnim.SetFloat("MoveX", 1);
							cabesaAnim.SetFloat("MoveY", 0);
						}

					} else {
						bullRb.velocity = Vector2.up * tearSpeed * Time.fixedDeltaTime * -1 + (rb.velocity / 2);
						if (cabesaAnim != null) {
							cabesaAnim.SetFloat("MoveX", 0);
							cabesaAnim.SetFloat("MoveY", -1);
						}
					}

				}
            }

			newBullet.transform.parent = transform.parent;
			bullRb.position = shootPos.position;

			Destroy(newBullet, 2f);
            Invoke(nameof(CanShootAgain), .2f);
        }

  //      if(cabesaAnim != null) {
		//	cabesaAnim.SetFloat("MoveX", horiz);
		//	cabesaAnim.SetFloat("MoveY", vertical);
		//}

	}

    private void CanShootAgain() {
        canShoot = true;
		if (cabesaAnim != null)
			cabesaAnim.SetBool("Shoot", false);
	}

    void FixedUpdate() {
		rb.velocity = new Vector2(moveX, moveY)*speed;
		if (moveX != 0) {
			if (patasAnim != null) {
				if (moveX < 0)
					transform.GetChild(1).localScale = new Vector3(-1, 1, 1);
				else
					transform.GetChild(1).localScale = new Vector3(1, 1, 1);
			}
		}
	}
}
