﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : NetworkBehaviour
{
    public float speed = 70;    
    public float bulletSpeed = 200;
    Rigidbody2D rbPJ;
    Vector2 movimiento;
    public GameObject tears;
    private bool cd = false;

	public void Start() {
		if (!isLocalPlayer) return;

		TryGetComponent<Rigidbody2D>(out rbPJ);
	}


	void Update() {
		if (!isLocalPlayer) return;

		movimiento.x = Input.GetAxisRaw("Horizontal");
		movimiento.y = Input.GetAxisRaw("Vertical");

        if (Input.GetKey("space"))
        {
           if (!cd)
           {
               Shoot();
               cd = true;
               Invoke(nameof(CoolCooldown), .5f);
           }
        }
    }

	void FixedUpdate()
    {
		if (!isLocalPlayer) return;
 
        rbPJ.MovePosition(rbPJ.position + movimiento * speed * Time.fixedDeltaTime);
    }

	private void CoolCooldown() {
		cd = false;
	}

    //private IEnumerator cooldown()
    //{
    //    cd = true;
    //    yield return new WaitForSeconds(0.5f); //Se habría de usar el Realtime por ser multi
    //    cd = false;
    //}

    [Client]
    private void Shoot()
    {
        commandShoot();
    }

    [Command]
    private void commandShoot()
    {
        GameObject newBullet = Instantiate(tears);
        newBullet.transform.position = this.transform.GetChild(0).position;
        //newBullet.GetComponent<Tears>().playerid = this.GetInstanceID();
        newBullet.GetComponent<Rigidbody2D>().velocity = newBullet.transform.up * bulletSpeed * Time.fixedDeltaTime;
        NetworkServer.Spawn(newBullet);
    }
}
