﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

/* pseudo-intefície que implementan los objetos que derivan de NetworkBehaviour 
 * y son golpeables 
 */
public abstract class Golpeable : NetworkBehaviour {

	public float maxHP; //vida máxima
	abstract public void Hit(float dmg); //función para dañar
	public Slider hpBar; //barra de vida

}
