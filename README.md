M17 Multiplayer con Mirror
---

Risketos Mínimos

- [X] Client i servidor clarament separats i diferenciats
- [X] Comandes úniques de client i comandes úniques de servidor
- [X] Mínim de 2 variables sincronitzades, amb els seus hooks
- [X] Ha d’haver missatges de client a servidor per a que executi (commands) i missatges de servidor a client per a que executi (ClientRpc o TargetRpc)
- [ ] Sistema de Comunicació client-client. Xat, emoticones, frases predefinides, etc
- [X] Canvi d’escenes amb un sistema de lobby
- [X] Mínim de 2 Objectes spawnables degudament registrats

risketos Adicionales

- [x] RoomManager
- [x] NetworkAnimator
- [x] Animaciones avanzadas con blend trees y eventos
