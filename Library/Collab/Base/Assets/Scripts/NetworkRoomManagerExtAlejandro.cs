using UnityEngine;
using Mirror;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

[AddComponentMenu("")]
    public class NetworkRoomManagerExtAlejandro : NetworkRoomManager
    {
    internal static readonly List<IsaacControllerNetwork> playersList = new List<IsaacControllerNetwork>();

    /// <summary>
    /// This is called on the server when a networked scene finishes loading.
    /// </summary>
    /// <param name="sceneName">Name of the new scene.</param>
    public override void OnRoomServerSceneChanged(string sceneName)
        {
           
        }

        /// <summary>
        /// Called just after GamePlayer object is instantiated and just before it replaces RoomPlayer object.
        /// This is the ideal point to pass any data like player name, credentials, tokens, colors, etc.
        /// into the GamePlayer object as it is about to enter the Online scene.
        /// </summary>
        /// <param name="roomPlayer"></param>
        /// <param name="gamePlayer"></param>
        /// <returns>true unless some code in here decides it needs to abort the replacement</returns>
        public override bool OnRoomServerSceneLoadedForPlayer(NetworkConnection conn, GameObject roomPlayer, GameObject gamePlayer)
        {
            gamePlayer.transform.position = startPositions[roomPlayer.GetComponent<NetworkRoomPlayer>().index].position;
            IsaacControllerNetwork p = gamePlayer.GetComponent<IsaacControllerNetwork>();
            p.NumPlayers = roomPlayer.GetComponent<NetworkRoomPlayer>().index;
            playersList.Add(p);

            //if (numPlayers == 1)
            //{
            //    gurdy.StartBattle(playersList);
            //}
            //if (numPlayers == 2 || numPlayers == 3)
            //{
            //    gurdy.AddPlayer(p);
            //}

        return true;
        }

        public override void OnRoomStopClient()
        {
            base.OnRoomStopClient();
        }

        public override void OnRoomStopServer()
        {
            base.OnRoomStopServer();
        }

        /*
            This code below is to demonstrate how to do a Start button that only appears for the Host player
            showStartButton is a local bool that's needed because OnRoomServerPlayersReady is only fired when
            all players are ready, but if a player cancels their ready state there's no callback to set it back to false
            Therefore, allPlayersReady is used in combination with showStartButton to show/hide the Start button correctly.
            Setting showStartButton false when the button is pressed hides it in the game scene since NetworkRoomManager
            is set as DontDestroyOnLoad = true.
        */

        bool showStartButton;

        public override void OnRoomServerPlayersReady()
        {
            // calling the base method calls ServerChangeScene as soon as all players are in Ready state.
            ServerChangeScene(GameplayScene);
        }        
    }

